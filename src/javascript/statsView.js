import App from "./app";
import View from "./view";

const view = new View();

function statsView(victories) {
  App.rootElement.innerHTML = "";
  const stats = view.createElement({
    tagName: "div",
    className: "stats-wrap"
  });
  const header = view.createElement({
    tagName: "h2",
    className: "stats-header"
  });
  const list = view.createElement({
    tagName: "ul",
    className: "stats-list"
  });
  const back = view.createElement({
    tagName: "button",
    className: "back-to-app"
  });
  victories.map(item => {
    const listItem = view.createElement({
      tagName: "li",
      className: "stats-item"
    });
    const itemName = view.createElement({
      tagName: "p",
      className: "stats-item-name"
    });
    const itemVictories = view.createElement({
      tagName: "p",
      className: "stats-item-victories"
    });
    itemName.innerText = `${item.name}`;
    itemVictories.innerText = `${item.victories}`;
    listItem.append(itemName, itemVictories);
    list.appendChild(listItem);
  });
  back.innerText = `Back`;
  header.innerText = `History of victories`;
  stats.append(header, list, back);
  App.rootElement.appendChild(stats);
  back.addEventListener("click", e => {
    App.rootElement.innerHTML = "";
    new App();
  });
}

export default statsView;
