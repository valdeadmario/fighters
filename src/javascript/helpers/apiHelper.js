const API_URL = "https://api-fighter.herokuapp.com/";

function callApi(endpoind, method, body) {
  const url = API_URL + endpoind;
  let headers;
  if (body) {
    body = JSON.stringify(body);
  }
  headers = {
    Accept: "application/json",
    "Content-Type": "application/json"
  };
  const options = {
    method,
    headers,
    body
  };
  return fetch(url, options)
    .then(response =>
      response.ok ? response.json() : Promise.reject(Error("Failed to load"))
    )
    .catch(error => {
      throw error;
    });
}

export { callApi };
