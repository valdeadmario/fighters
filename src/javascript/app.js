import View from "./view";
import FightersView from "./fightersView";
import { fighterService } from "./services/fightersService";
import Fight from "./fight";
import statsView from "./statsView";

class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById("root");
  static loadingElement = document.getElementById("loading-overlay");

  async startApp() {
    try {
      App.loadingElement.style.visibility = "visible";

      const fighters = await fighterService.getFighters();
      const victories = await fighterService.getVictories();
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;

      const view = new View();

      const button = view.createElement({
        tagName: "button",
        className: "start-game"
      });
      button.innerText = "Start Game";

      const buttonToStats = view.createElement({
        tagName: "button",
        className: "stats"
      });
      buttonToStats.innerText = "View vistories stats";

      App.rootElement.appendChild(fightersElement);
      App.rootElement.appendChild(button);
      App.rootElement.appendChild(buttonToStats);

      button.addEventListener("click", e => {
        e.preventDefault();
        if (fightersView.fighterToFight.length === 2) {
          App.rootElement.innerHTML = "";
          new Fight(...fightersView.fighterToFight);
        } else {
          alert("Can choice only 2 fighters!");
        }
      });

      buttonToStats.addEventListener("click", e => {
        e.preventDefault();
        statsView(victories);
      });
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = "Failed to load data";
    } finally {
      App.loadingElement.style.visibility = "hidden";
    }
  }
}

export default App;
