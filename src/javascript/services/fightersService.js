import { callApi } from "../helpers/apiHelper";

class FighterService {
  async getFighters() {
    try {
      const endpoint = "users";
      const apiResult = await callApi(endpoint, "GET");

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(_id) {
    try {
      const endpoint = `users/${_id}`;
      const apiResult = await callApi(endpoint, "GET");

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async putFighter(_id, fighter) {
    try {
      const endpoint = `users/${_id}`;
      const apiResult = await callApi(endpoint, "PUT", fighter);
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getVictories() {
    try {
      const endpoint = "victories";
      const apiResult = await callApi(endpoint, "GET");

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async putVictory(_id) {
    try {
      const endpoint = `victories/${_id}`;
      const apiResult = await callApi(endpoint, "PUT");

      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
